package me.palombo.core.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public static Connection con;
    
    public static void connect() {
        try {
			con = DriverManager.getConnection("jdbc:mysql://185.46.120.194:3306/palomboe_server", "palomboe_palombo",
					"password1");
            System.out.println("[TNTWARS-CORE] The connection to MySQL is made!");
        } catch (SQLException e) {
            System.out.println("[TNTWARS-CORE] The connection to MySQL couldn't be made! reason: " + e.getMessage());
        }
    }

    public static void close() {
        try {
            if (con != null) {
                con.close();
                System.out.println("[TNTWARS-CORE] The connection to MySQL is ended successfully!");
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public static PreparedStatement prepareStatement(String qry) {
    	if (noConnection()){
    		connect();
    	}
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(qry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;
    }
    public static void update(PreparedStatement statement) {
    	if (noConnection()){
    		connect();
    	}
        try {
            statement.executeUpdate();    
        } catch (SQLException e) {
            connect();
            e.printStackTrace();
        }finally{
           try {
             statement.close();
           } catch (SQLException e) {
             e.printStackTrace();
           }
        }
    }

    public static boolean noConnection() {
        return con == null;
    }

    public static ResultSet query(PreparedStatement statement) {
    	if (noConnection()){
    		connect();
    	}
        try {
            return statement.executeQuery();
        } catch (SQLException e) {
            connect();
            e.printStackTrace();
        }
        return null;
    }
}
