package me.palombo.core.message.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.account.Account;
import me.palombo.core.message.Message;
import me.palombo.core.rank.StaffRank;

public class SocialSpyCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("socialspy")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				if (account.hasStaffRank(StaffRank.MOD, true)) {
					if (Message.socialSpyUsers.contains(player.getName())) {
						Message.socialSpyUsers.remove(player.getName());
						player.sendMessage("�c�lMessage �7Social Spy Disabled.");
					} else {
						Message.socialSpyUsers.add(player.getName());
						player.sendMessage("�c�lMessage �7Social Spy Enabled.");
					}
				}

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}
}
