package me.palombo.core.message.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.message.Message;

public class MessageCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("message")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (args.length < 2) {
					usageMessage(player);
				} else {
					if (Bukkit.getPlayer(args[0]) != null) {
						Player target = Bukkit.getPlayer(args[0]);

						String message = "";
						for(int i = 1; i < args.length ; i++){
							message += args[i] + " "; 
						}

						player.sendMessage("�7�l" + player.getName() + " �8-> �7�l" + target.getName() + " �7" + message.trim());
						target.sendMessage("�7�l" + target.getName() + " �8-> �7�l" + player.getName() + " �7" + message.trim());
						
						for (String s : Message.socialSpyUsers) {
							Player sp = Bukkit.getPlayer(s);
							
							sp.sendMessage("�c�lSocial Spy �7�l" + player.getName() + " �8-> �7�l" + target.getName() + " �7" + message.trim());
						}
						
						Message.talkingTo.put(player.getName(), target.getName());
						Message.talkingTo.put(target.getName(), player.getName());
					} else {
						player.sendMessage("�c�lPing �7" + args[0] + " �7was not found.");
					}

				}

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/message [player] [message]");
		player.sendMessage("�8�m�l------------------------------");
	}
}
