package me.palombo.core.message.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.message.Message;

public class ReplyCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("reply")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (args.length == 0) {
					usageMessage(player);
				} else {
					if (Bukkit.getPlayer(Message.talkingTo.get(player.getName())) != null) {
						Player target = Bukkit.getPlayer(Message.talkingTo.get(player.getName()));

						String message = "";
						for(int i = 0; i < args.length ; i++){
							message += args[i] + " "; 
						}

						player.sendMessage("�7�l" + player.getName() + " �8-> �7�l" + target.getName() + " �7" + message.trim());
						target.sendMessage("�7�l" + target.getName() + " �8-> �7�l" + player.getName() + " �7" + message.trim());
						
						for (String s : Message.socialSpyUsers) {
							Player sp = Bukkit.getPlayer(s);
							
							sp.sendMessage("�c�lSocial Spy �7�l" + player.getName() + " �8-> �7�l" + target.getName() + " �7" + message.trim());
						}
					} else {
						player.sendMessage("�c�lPing �7You have nobody to reply to.");
					}

				}

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/reply [message]");
		player.sendMessage("�8�m�l------------------------------");
	}
}
