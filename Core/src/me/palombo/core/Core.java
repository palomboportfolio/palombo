package me.palombo.core;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import me.palombo.core.account.Account;
import me.palombo.core.account.JoinQuit;
import me.palombo.core.account.command.DonorRankCMD;
import me.palombo.core.account.command.OwnerCMD;
import me.palombo.core.account.command.StaffRankCMD;
import me.palombo.core.chat.Chat;
import me.palombo.core.chat.command.ChatCMD;
import me.palombo.core.coins.CoinsCMD;
import me.palombo.core.database.MySQL;
import me.palombo.core.level.LevelCMD;
import me.palombo.core.level.listener.LevelListener;
import me.palombo.core.message.command.MessageCMD;
import me.palombo.core.message.command.ReplyCMD;
import me.palombo.core.message.command.SocialSpyCMD;
import me.palombo.core.npc.NPC;
import me.palombo.core.npc.command.NPCCMD;
import me.palombo.core.npc.listener.NPCListener;
import me.palombo.core.ping.PingCMD;
import me.palombo.core.punish.PunishMenu;
import me.palombo.core.punish.command.PunishCMD;
import me.palombo.core.rank.DonorRank;
import me.palombo.core.rank.StaffRank;
import me.palombo.core.rules.command.RulesCMD;
import me.palombo.core.scoreboard.ScoreboardManager;
import me.palombo.core.server.command.ServerCMD;
import me.palombo.core.server.menu.ReloadConfirmMenu;
import me.palombo.core.server.menu.StopConfirmMenu;
import me.palombo.core.teleport.command.TeleportCMD;

public class Core extends JavaPlugin {
	
	public static HashMap<UUID, StaffRank> staffrank = new HashMap<UUID, StaffRank>();
	public static HashMap<UUID, DonorRank> donorrank = new HashMap<UUID, DonorRank>();
	public static HashMap<UUID, Integer> coins = new HashMap<UUID, Integer>();
	public static HashMap<UUID, Integer> level = new HashMap<UUID, Integer>();
	
	public static HashMap<UUID, HashMap<String, String>> muted = new HashMap();

	public static Core instance;

	public void onEnable() {
		instance = this;
		
		//Location location = new Location(getConfig().getString("npcs"))

		//NPC.spawnNPC(location, entityType, npcName, displayName);
		
		MySQL.connect();
	
		new BukkitRunnable() {
			public void run() {
				if (MySQL.noConnection()) {
					MySQL.connect();
				}
			}
		}.runTaskTimer(this, 0, 20L * 30);

		getConfig().options().copyDefaults(true);
		saveDefaultConfig();

		registerListener(new JoinQuit());
		registerListener(new Chat());
		registerListener(new TestCMD(this));
		registerListener(new ReloadConfirmMenu());
		registerListener(new StopConfirmMenu());
		registerListener(new LevelListener());
		registerListener(new NPCListener());
		registerListener(new NPC(this));
		registerListener(new ScoreboardManager());
		registerListener(new PunishMenu());

		registerCommand("test", new TestCMD(this));
		registerCommand("chat", new ChatCMD());
		registerCommand("staffrank", new StaffRankCMD());
		registerCommand("donorrank", new DonorRankCMD());
		registerCommand("owner", new OwnerCMD());
		registerCommand("teleport", new TeleportCMD());
		registerCommand("server", new ServerCMD());
		registerCommand("punish", new PunishCMD());
		registerCommand("coins", new CoinsCMD());
		registerCommand("level", new LevelCMD());
		registerCommand("ping", new PingCMD());
		registerCommand("message", new MessageCMD());
		registerCommand("reply", new ReplyCMD());
		registerCommand("socialspy", new SocialSpyCMD());
		registerCommand("rules", new RulesCMD());
		registerCommand("npc", new NPCCMD());

		if (Bukkit.getOnlinePlayers().size() > 0) {
			for (Player player : Bukkit.getOnlinePlayers()) {
				Account account = new Account(player);
				
				account.loadAccount();
			}
		}
	}

	public void onDisable() {

		for (Entity entity : NPC.npcs.values()) {
			entity.remove();
		}

		instance = null;
	}
	
	public static boolean isInt(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
    public static String timeFormat(int time, String color) {
        if (time == -1) {
            return color + "PERMANENT";
        }
        if (time == 0) {
            return color + "REMOVED";
        }
        long days = TimeUnit.SECONDS.toDays(time);
        time = (int)((long)time - TimeUnit.DAYS.toSeconds(days));
        long hours = TimeUnit.SECONDS.toHours(time);
        time = (int)((long)time - TimeUnit.HOURS.toSeconds(hours));
        long minutes = TimeUnit.SECONDS.toMinutes(time);
        time = (int)((long)time - TimeUnit.MINUTES.toSeconds(minutes));
        long seconds = TimeUnit.SECONDS.toSeconds(time);
        return color + timeFormat((int)days, (int)hours, (int)minutes, (int)seconds);
    }

    public static String timeFormat(int days, int hours, int minutes, int seconds) {
        String r = "";
        if (days != 0) {
            r = days == 1 ? r + days + " day " : r + days + " days ";
        }
        if (hours != 0) {
            r = hours == 1 ? r + hours + " hour " : r + hours + " hours ";
        }
        if (minutes != 0) {
            r = minutes == 1 ? r + minutes + " minute " : r + minutes + " minutes ";
        }
        if (seconds != 0) {
            r = seconds == 1 ? r + seconds + " second " : r + seconds + " seconds ";
        }
        r = r.substring(0, r.length() - 1);
        return r;
    }

	public void registerListener(Listener... listener) {
		for (Listener l : listener) {
			this.getServer().getPluginManager().registerEvents(l, this);
		}
	}

	public void registerCommand(String command, CommandExecutor commandExecutor) {
		this.getCommand(command).setExecutor(commandExecutor);
	}

	public static Core getInstance() {
		return instance;
	}

}
