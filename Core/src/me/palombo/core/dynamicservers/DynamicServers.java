package me.palombo.core.dynamicservers;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.mysql.jdbc.PreparedStatement;

import me.palombo.core.Core;
import me.palombo.core.database.MySQL;

public class DynamicServers implements Listener {

	public static String serverName = Core.getInstance().getConfig().getString("server-name");
	public static String serverType = Core.getInstance().getConfig().getString("server-type");
	public static String mapName = Core.getInstance().getConfig().getString("map-name");

	public static void checkServer() {
		if (isInDatabase() == false) {
			try {
				PreparedStatement newServer = (PreparedStatement) MySQL.con
						.prepareStatement("INSERT INTO `DynamicServers` values(?,?,?,?,?);");
				newServer.setString(1, serverName);
				newServer.setString(2, mapName);
				newServer.setInt(3, 0);
				newServer.setString(4, "ONLINE");
				newServer.setString(5, serverType);
				newServer.execute();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		setCount(Bukkit.getOnlinePlayers().size());
		setMap(mapName);
		setStatus("ONLINE");
		setType(serverType);

	}

	public static void setServerName(String serverName) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT NAME FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET NAME=? WHERE NAME=?;");
			Save.setString(1, serverName);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setMap(String map) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT MAP FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET MAP=? WHERE NAME=?;");
			Save.setString(1, map);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setCount(int count) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT COUNT FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET COUNT=? WHERE NAME=?;");
			Save.setInt(1, count);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setStatus(String status) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT STATUS FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET STATUS=? WHERE NAME=?;");
			Save.setString(1, status);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setType(String type) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT TYPE FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET TYPE=? WHERE NAME=?;");
			Save.setString(1, type);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setWinners(ArrayList<String> winners) {
		try {
			PreparedStatement sql2 = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT TYPE FROM `DynamicServers` WHERE NAME=?;");
			sql2.setString(1, serverName);
			PreparedStatement Save = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `DynamicServers` SET TYPE=? WHERE NAME=?;");
			Save.setArray(1, (Array) winners);
			Save.setString(2, serverName);
			Save.executeUpdate();
			Save.close();
			sql2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getServerName() {
		try {
			ResultSet r = MySQL.con.createStatement().executeQuery(
					"SELECT NAME FROM DynamicServers WHERE NAME = '" + serverName + "'");
			if (r.next()) {
				return r.getString("NAME");
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getMap() {
		try {
			ResultSet r = MySQL.con.createStatement().executeQuery(
					"SELECT MAP FROM DynamicServers WHERE NAME = '" + serverName + "'");
			if (r.next()) {
				return r.getString("MAP");
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int getCount() {
		try {
			ResultSet r = MySQL.con.createStatement().executeQuery(
					"SELECT COUNT FROM DynamicServers WHERE NAME = '" + serverName + "'");
			if (r.next()) {
				return r.getInt("COUNT");
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static String getStatus() {
		try {
			ResultSet r = MySQL.con.createStatement().executeQuery(
					"SELECT STATUS FROM DynamicServers WHERE NAME = '" + serverName + "'");
			if (r.next()) {
				return r.getString("STATUS");
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getType() {
		try {
			ResultSet r = MySQL.con.createStatement().executeQuery(
					"SELECT TYPE FROM DynamicServers WHERE NAME = '" + serverName + "'");
			if (r.next()) {
				return r.getString("TYPE");
			}
			r.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean isInDatabase() {
		try {
			PreparedStatement sql = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT * FROM `DynamicServers` WHERE NAME=?;");
			sql.setString(1, serverName);

			ResultSet resultSet = sql.executeQuery();
			boolean contains = resultSet.next();

			sql.close();
			resultSet.close();

			return contains;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void removeServer(String name) {
		if (isInDatabase() == true) {
			try {

				PreparedStatement sql = (PreparedStatement) MySQL.con
						.prepareStatement("DELETE FROM DynamicServers WHERE NAME =('" + name + "')");

				sql.executeUpdate();
				sql.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*public static String getAvailableServer() {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM DynamicServers");
            while (r.next()) {
                String value = r.getString("STATUS");
                if (value.contains("AVAILABLE")) {
                    return r.getString(1);
                }
            }
            r.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void sendToAvailableServer(Player player) {
        if (getAvailableServer() != null) {

            alreadyTried = false;

            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF(getAvailableServer());
            player.sendPluginMessage(Main.inst(), "BungeeCord", out.toByteArray());

            try {
                PreparedStatement sql2 = (PreparedStatement) MySQL.con
                        .prepareStatement("SELECT STATUS FROM `DynamicServers` WHERE NAME=" + getAvailableServer() + ";");

                PreparedStatement Save = (PreparedStatement) MySQL.con
                        .prepareStatement("UPDATE `DynamicServers` SET STATUS=? WHERE NAME =('" + getAvailableServer() + "')");
                Save.setString(1, "RESERVED");
                Save.executeUpdate();
                Save.close();
                sql2.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            if (alreadyTried == false) {
                alreadyTried = true;

                after15Seconds(player);
            } else {
                alreadyTried = false;
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF("hub");
                player.sendPluginMessage(Main.inst(), "BungeeCord", out.toByteArray());

                EmergencyNotify.email("We are out of Win Effect servers! We were forced to send players to the hub.", "Out of Win Effect servers", "2082063734@messaging.sprintpcs.com", "tntwarsofficial@gmail.com", "M&z6EABzWk");
                EmergencyNotify.email("We are out of Win Effect servers! We were forced to send players to the hub.", "Out of Win Effect servers", "3375177393@mms.att.net", "tntwarsofficial@gmail.com", "M&z6EABzWk");
            }
        }
    }

    public static void after15Seconds(Player player) {
        new BukkitRunnable() {

            public void run() {
                sendToAvailableServer(player);
            }
        }.runTaskLater(Main.getInstance(), 20L * 15);
    }*/


	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		setCount(Bukkit.getOnlinePlayers().size());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		setCount(Bukkit.getOnlinePlayers().size() - 1);
	}
}