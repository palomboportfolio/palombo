package me.palombo.core.teleport.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class TeleportCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("teleport")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.MOD, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args.length == 1) {
							if (Bukkit.getPlayer(args[0]) == null || Bukkit.getPlayer(args[0]).isOnline() == false) {
								player.sendMessage("�c�lTeleport �7" + args[0] + " was not found.");
							} else {
								Player target = Bukkit.getPlayer(args[0]);
								player.teleport(target);
								player.sendMessage("�c�lTeleport �7You have teleported to " + target.getName() + ".");
							}
						} else {
							if (account.hasStaffRank(StaffRank.DEV, true)) {
								if (Bukkit.getPlayer(args[0]) != null || args[0].equalsIgnoreCase("here")) {
									
									if (args[0].equalsIgnoreCase("here")) {
										if (Bukkit.getPlayer(args[1]) == null) {
											player.sendMessage("�c�lTeleport �7" + args[1] + " was not found.");
											return true;
										} else {
											Bukkit.getPlayer(args[1]).teleport(player);
											player.sendMessage("�c�lTeleport �7You have teleported " + args[1] + " to you.");
											return true;
										}
									}
									
									if (Bukkit.getPlayer(args[0]) == null) {
										player.sendMessage("�c�lTeleport �7" + args[0] + " was not found.");
									} else {
										if (Bukkit.getPlayer(args[1]) == null) {
											player.sendMessage("�c�lTeleport �7" + args[1] + " was not found.");
											return true;
										} else {
											Bukkit.getPlayer(args[0]).teleport(Bukkit.getPlayer(args[1]));
											player.sendMessage("�c�lTeleport �7You have teleported " + args[0] + " to " + args[1] + ".");
											return true;
										}
									}
								}
							} else {
								usageMessage(player);
							}
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/teleport [player]");
		player.sendMessage("  �7/teleport [player] [player]");
		player.sendMessage("  �7/teleport here [player]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
