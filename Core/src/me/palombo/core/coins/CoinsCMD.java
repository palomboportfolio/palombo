package me.palombo.core.coins;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.Core;
import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class CoinsCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("coins")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				
				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					if (args.length < 2) {
						usageMessage(player);
					} else {
						if (args.length >= 2) {
							if (args[0].equalsIgnoreCase("get") || args[0].equalsIgnoreCase("set")
									|| args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove")) {
								if (Bukkit.getPlayer(args[1]) != null) {
									Player target = Bukkit.getPlayer(args[1]);
									Account targetAccount = new Account(target);
									int targetCoinAmount = targetAccount.getCoins();

									if (args[0].equalsIgnoreCase("get")) {
										player.sendMessage("�c�lCoins �7" + args[1] + " has �7�l" + targetAccount.getCoins() + " �7coins.");
										return true;
									}

									if (args.length > 2) {
										if (Core.isInt(args[2])) {
											if (!(args[2].contains("-"))) {
												if (args[0].equalsIgnoreCase("set")) {
													targetAccount.setCoins(Integer.valueOf(args[2]));
													player.sendMessage("�c�lCoins �7" + args[1] + "'s coins have been set to �7�l" + args[2] + "�7.");
												} else if (args[0].equalsIgnoreCase("add")) {
													targetAccount.setCoins(targetCoinAmount + Integer.valueOf(args[2]));
													player.sendMessage("�c�lCoins �7�l" + args[2] + " �7coins have been added to " + args[1] + "�7.");
												} else if (args[0].equalsIgnoreCase("remove")) {
													if (Integer.valueOf(args[2]) > targetCoinAmount) {
														player.sendMessage("�c�lCoins �7�l" + args[1] + " �7has less than �7�l" + args[2] + "�7. So you can not take away that many.");
													} else {
														targetAccount.setCoins(targetCoinAmount - Integer.valueOf(args[2]));
														player.sendMessage("�c�lCoins �7�l" + args[2] + " �7coins have been removed from " + args[1] + "�7.");
													}
												}
											} else {
												player.sendMessage("�c�lCoins �7You must use a positive number for the amount.");
											}
										} else {
											player.sendMessage("�c�lCoins �7You must use a number for the amount.");
										}
									} else {
										usageMessage(player);
									}

								} else {
									player.sendMessage("�c�lCoins �7" + args[1] + " was not found.");
								}
							} else {
								usageMessage(player);
							}
						}
					}
				} else {
					player.sendMessage("�c�lCoins �7You have �7�l" + account.getCoins() + " �7coins.");
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/coins get [player]");
		player.sendMessage("  �7/coins set [player] [amount]");
		player.sendMessage("  �7/coins add [player] [amount]");
		player.sendMessage("  �7/coins remove [player] [amount]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
