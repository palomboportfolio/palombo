package me.palombo.core.rules;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Rules {

	public static String openRules(Player player, int page) {
		if (page == 1) {
			return ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "-------------------------------------\n" +
					ChatColor.AQUA + "1. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "spamming.\n" +
					ChatColor.AQUA + "2. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "griefing.\n" +
					ChatColor.AQUA + "2. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "hacking.\n" +
					ChatColor.AQUA + "4. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "spawn killing.\n" +
					ChatColor.AQUA + "5. " + ChatColor.RED + ChatColor.BOLD.toString() +"No  " + ChatColor.GRAY + "exploiting glitches.\n" +
					ChatColor.AQUA + "6. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "curse words or profanity.\n" +
					ChatColor.AQUA + "Use /rules 2 for page 2!\n" + 
					ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "-------------------------------------";
		} else {
			return ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "-------------------------------------\n" +
					ChatColor.AQUA + "7. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "begging for things from staff.\n" +
					ChatColor.AQUA + "8. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "AFK farming. (Farming for online time)\n" +
					ChatColor.AQUA + "9. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "Water protection.\n" +
					ChatColor.AQUA + "10. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "advertising.\n" +
					ChatColor.AQUA + "11. " + ChatColor.RED + ChatColor.BOLD.toString() +"No " + ChatColor.GRAY + "debating with staff members.\n" +
					ChatColor.AQUA + "12. " + ChatColor.GOLD + ChatColor.BOLD.toString() +"Always have fun!\n" +
					ChatColor.GREEN + "For more information on the rules, visit " + ChatColor.AQUA + ChatColor.BOLD.toString() + "tntwars.net\n" +
					ChatColor.AQUA + ChatColor.STRIKETHROUGH.toString() + "-------------------------------------";
		}
	}

}