package me.palombo.core.rules.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.rules.Rules;

public class RulesCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("rules")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				
				if (args.length == 0) {
					player.sendMessage(Rules.openRules(player, 1));
				} else {
					player.sendMessage(Rules.openRules(player, 2));
				}

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}
}
