package me.palombo.core.scoreboard;

import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ScoreboardManager implements Listener {	

	//This stores current scoreboard for the player
	private HashMap<Player, PlayerScoreboard> _playerScoreboards = new HashMap<Player, PlayerScoreboard>();

	//Scoreboards (can be shared between players)
	private HashMap<String, ScoreboardData> _scoreboards = new HashMap<String, ScoreboardData>();

	//Title
	private String _title = "   MINEPLEX   ";
	private int _shineIndex;
	private boolean _shineDirection = true;


	@EventHandler
	public void playerJoin(PlayerJoinEvent event)
	{
		_playerScoreboards.put(event.getPlayer(), new PlayerScoreboard(this, event.getPlayer()));
	}

	@EventHandler
	public void playerQuit(PlayerQuitEvent event)
	{
		_playerScoreboards.remove(event.getPlayer());
	}

	public void draw()
	{
		Iterator<Player> playerIterator = _playerScoreboards.keySet().iterator();

		while (playerIterator.hasNext())
		{
			Player player = playerIterator.next();

			//Offline
			if (!player.isOnline())
			{
				playerIterator.remove();
				continue;
			}

			_playerScoreboards.get(player).draw(this, player);
		}
	}


	
	public ScoreboardData getData(String scoreboardName, boolean create)
	{
		if (!create)
			return _scoreboards.get(scoreboardName);

		if (!_scoreboards.containsKey(scoreboardName))
			_scoreboards.put(scoreboardName, new ScoreboardData());

		return _scoreboards.get(scoreboardName);
	}
}
