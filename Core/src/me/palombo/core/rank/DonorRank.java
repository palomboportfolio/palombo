package me.palombo.core.rank;

import org.bukkit.ChatColor;

public enum DonorRank {

    DIAMOND(ChatColor.AQUA.toString() + ChatColor.BOLD + "Diamond"),
    EMERALD(ChatColor.GREEN.toString() + ChatColor.BOLD + "Emerald"),
    GOLD(ChatColor.GOLD.toString() + ChatColor.BOLD + "Gold"),
    IRON(ChatColor.GRAY.toString() + ChatColor.BOLD + "Iron"),
    COAL(ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "Coal"),
    DEFAULT("");

    private String prefix;

    DonorRank(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}