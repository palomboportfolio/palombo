package me.palombo.core.rank;

import org.bukkit.ChatColor;

public enum StaffRank {

    OWNER(ChatColor.RED.toString() + ChatColor.BOLD + "Owner"),
    LEADDEV(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Lead Dev"),
    DEV(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Dev"),
    JRDEV(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Jr. Dev"),
    ADMIN(ChatColor.GREEN.toString() + ChatColor.BOLD + "Admin"),
    MODPLUS(ChatColor.GOLD.toString() + ChatColor.BOLD + "Mod+"),
    MOD(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Mod"),
    HELPER(ChatColor.BLUE.toString() + ChatColor.BOLD + "Helper"),
    YOUTUBE(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "You" + ChatColor.WHITE.toString() + ChatColor.BOLD + "Tube"),
    NONE("");

    private String prefix;

    StaffRank(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}