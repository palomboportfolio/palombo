package me.palombo.core;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.palombo.core.account.Account;
import me.palombo.core.punish.PunishMenu;

public class TestCMD implements CommandExecutor, Listener {
	
	private static Core core;
	public TestCMD(Core core) {
		this.core = core;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("test")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				player.sendMessage(PunishMenu.punish.get(player.getName()) + "");

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}
}
