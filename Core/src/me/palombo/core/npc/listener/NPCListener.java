package me.palombo.core.npc.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import me.palombo.core.npc.NPC;

public class NPCListener implements Listener {

	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if (NPC.npcs.values().contains(event.getEntity())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (NPC.npcs.values().contains(event.getEntity())) {
			if (event.getDamager().getType() == EntityType.ARROW) {
				event.setCancelled(true);
			} else {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onEntityCombust(EntityCombustEvent event){
		if (event.getEntity() instanceof Zombie){
			if (NPC.npcs.values().contains(event.getEntity())) {
				event.setCancelled(true);
			}
		}
	}

}
