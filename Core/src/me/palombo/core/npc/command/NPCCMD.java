package me.palombo.core.npc.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.account.Account;
import me.palombo.core.npc.NPC;
import me.palombo.core.npc.NPCTYPE;
import me.palombo.core.rank.StaffRank;

public class NPCCMD implements CommandExecutor, Listener {


	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("npc")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.DEV, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args.length == 1) {
							if (args[0].equalsIgnoreCase("list")) {
								for (String s : NPC.npcNames) {
									player.sendMessage(s);
								}
							} else {
								return true;
							}
						} else if (args.length > 1) {
							if (args[0].equalsIgnoreCase("create")) {
								if (args.length < 4) {
									usageMessage(player);
								} else {
									if (EntityType.valueOf(args[1].toUpperCase()) != null && NPCTYPE.valueOf(args[1]) != null) {
										if (!(NPC.npcs.containsKey(args[2]))) {
											if (args[3] != null) {
												NPC.spawnNPC(player.getLocation(), EntityType.valueOf(args[1].toUpperCase()), args[2], args[3]);
											} else {
												player.sendMessage("�c�lNPC �7Invalid NPC Name.");
											}
										} else {
											player.sendMessage("�c�lNPC �7This NPC already exist.");
										}
									} else {
										player.sendMessage("�c�lNPC �7Invalid NPC Type.");
									}
								}
							} else if (args[0].equalsIgnoreCase("delete")) {
								if (args.length < 2) {
									usageMessage(player);
								} else{
									if (NPC.npcNames.contains(args[1])) {
										Entity npc = NPC.npcs.get(args[1]);
										npc.remove();
										NPC.npcs.remove(args[1]);
										NPC.npcNames.remove(args[1]);
										player.sendMessage("�c�lNPC �7The NPC �7�l" + args[1] + " �7has been removed.");
									} else {
										player.sendMessage("�c�lNPC �7This NPC does not exist.");
									}
								}
							} else if (args[0].equalsIgnoreCase("goto")) {
								if (args.length < 2) {
									usageMessage(player);
								} else{
									if (NPC.npcNames.contains(args[1])) {
										Entity npc = NPC.npcs.get(args[1]);
										player.teleport(npc.getLocation());
									} else {
										player.sendMessage("�c�lNPC �7This NPC does not exist.");
									}
								}
							} else if (args[0].equalsIgnoreCase("types")) {
								player.sendMessage(NPCTYPE.values().toString());
							} else {
								usageMessage(player);
							}
						}
					}
				}

			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/npc list");
		player.sendMessage("  �7/npc create [type] [name] [displayname] [set equipment] [set inhand]");
		player.sendMessage("  �7/npc delete [name]");
		player.sendMessage("  �7/npc goto [name]");
		player.sendMessage("  �7/npc types");
		player.sendMessage("�8�m�l------------------------------");
	}
}
