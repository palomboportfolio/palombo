package me.palombo.core.npc;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Listener;

import me.palombo.core.Core;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class NPC implements Listener {
	
	private static Core core;
	
	public NPC(Core core) {
		NPC.core = core;
	}
	
	public static HashMap<String, Entity> npcs = new HashMap<>();
	
	public static ArrayList<String> npcNames = new ArrayList<>();

	public static void spawnNPC(Location location, EntityType entityType, String npcName, String displayName) {
			Entity npc = (Entity) location.getWorld().spawnEntity(location, entityType);
			core.getConfig().set("npcs." + npcName + ".type", entityType.toString());
			//EntityEquipment ee = ((LivingEntity) npc).getEquipment();
			/*if (setYourEquipment) {
				ee.setArmorContents(player.getInventory().getArmorContents());
				 
				List<ItemStack[]> inventoryContents = new ArrayList<ItemStack[]>();
				inventoryContents.add(player.getInventory().getArmorContents());
				
				Core.getInstance().getConfig().set("npc.equipment", inventoryContents);  

			}
			if (setYourItemInHand) {
				ee.setItemInHand(player.getItemInHand());
				
				List<ItemStack> inhand = new ArrayList<ItemStack>();
				inhand.add(player.getItemInHand());
				
				Core.getInstance().getConfig().set("npc.equipment", inhand); 
			}*/
			if (displayName != null) {
				npc.setCustomName(displayName);
				npc.setCustomNameVisible(true);
				core.getConfig().set("npcs." + npcName + ".displayname", displayName);
			}
			noAI((Entity) npc);
			npcs.put(npcName, npc);
			npcNames.add(npcName);
			
			core.getConfig().set("npcs." + npcName + ".location.world", location.getWorld().getName());
			core.getConfig().set("npcs." + npcName + ".location.x", location.getX());
			core.getConfig().set("npcs." + npcName + ".location.y", location.getY());
			core.getConfig().set("npcs." + npcName + ".location.z", location.getZ());
			core.getConfig().set("npcs." + npcName + ".location.pitch", location.getPitch());
			core.getConfig().set("npcs." + npcName + ".location.yaw", location.getYaw());
			
			core.saveConfig();
	}

	public static void noAI(Entity bukkitEntity) {
		net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) bukkitEntity).getHandle();
		NBTTagCompound tag = nmsEntity.getNBTTag();
		if (tag == null) {
			tag = new NBTTagCompound();
		}
		nmsEntity.c(tag);
		tag.setInt("NoAI", 1);
		nmsEntity.f(tag);
	}

}
