package me.palombo.core.punish;

public enum PunishType {
	WARN,
	KICK,
	TEMPMUTE,
	MUTE,
	TEMPBAN,
	BAN;
}
