package me.palombo.core.punish.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.punish.PunishMenu;
import me.palombo.core.rank.StaffRank;

public class PunishCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("punish")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				if (account.hasStaffRank(StaffRank.HELPER, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (Bukkit.getPlayer(args[0]) != null) {
							Player target = Bukkit.getPlayer(args[0]);
							
							PunishMenu.punish.put(player.getName(), target.getName());
							PunishMenu.openPunishMenu(player, target);		
						} else {
							player.sendMessage("�c�lPunish �7" + args[0] + " was not found.");
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}
		
		public void usageMessage(Player player) {
			player.sendMessage("�8�m�l------------------------------");
			player.sendMessage("  �7/punish [player]");
			player.sendMessage("�8�m�l------------------------------");
		}

}
