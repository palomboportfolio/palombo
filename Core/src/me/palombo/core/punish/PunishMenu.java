package me.palombo.core.punish;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.palombo.core.account.Account;
import me.palombo.core.database.MySQL;

public class PunishMenu implements Listener {

	public static Inventory punishMenu;

	public static void openPunishMenu(Player player, Player target) {
		punishMenu = Bukkit.createInventory(player, 54, "Punish Menu");

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
		skullMeta.setOwner(target.getName());
		skullMeta.setDisplayName("�c�lTarget: �7" + target.getName());
		skull.setItemMeta(skullMeta);
		punishMenu.setItem(4, skull);
		
		ItemStack chatOffense = new ItemStack(Material.SIGN, 1);
		ItemMeta chatOffenseMeta = chatOffense.getItemMeta();
		chatOffenseMeta.setDisplayName("�6�lChat Offense");
		ArrayList<String> chatOffenseLore = new ArrayList<String>();
		chatOffenseLore.add("�7Verbal Abuse, Spam, Harassment, Trolling, etc");
		chatOffenseMeta.setLore(chatOffenseLore);
		chatOffense.setItemMeta(chatOffenseMeta);
		punishMenu.setItem(19, chatOffense);
		
		ItemStack chatOffenseSev1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta chatOffenseSev1Meta = chatOffenseSev1.getItemMeta();
		chatOffenseSev1Meta.setDisplayName("�a�lSeverity 1");
		ArrayList<String> chatOffenseSev1Lore = new ArrayList<String>();
		chatOffenseSev1Lore.add("�9�lMute Duration: �72 Hours");
		chatOffenseSev1Lore.add(" ");
		chatOffenseSev1Lore.add("�f�lExamples:");
		chatOffenseSev1Lore.add("�7Spamming same thing in chat (3-5 times)");
		chatOffenseSev1Lore.add("�7Light Advertising");
		chatOffenseSev1Lore.add("�7Trolling");
		chatOffenseSev1Lore.add("�7Constantly talking garbage");
		chatOffenseSev1Lore.add("�7Pestering staff");
		chatOffenseSev1Lore.add("�7Accusing a player of hacks in chat");
		chatOffenseSev1Lore.add(" ");
		chatOffenseSev1Lore.add("�5�nUse Severity 2 if many Severity 1 past offences");
		chatOffenseSev1Meta.setLore(chatOffenseSev1Lore);
		chatOffenseSev1.setItemMeta(chatOffenseSev1Meta);
		punishMenu.setItem(20, chatOffenseSev1);
		
		ItemStack chatOffenseSev2 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4);
		ItemMeta chatOffenseSev2Meta = chatOffenseSev2.getItemMeta();
		chatOffenseSev2Meta.setDisplayName("�e�lSeverity 2");
		ArrayList<String> chatOffenseSev2Lore = new ArrayList<String>();
		chatOffenseSev2Lore.add("�9�lMute Duration: �724 Hours");
		chatOffenseSev2Lore.add(" ");
		chatOffenseSev2Lore.add("�f�lExamples:");
		chatOffenseSev2Lore.add("�7Spamming same thing in chat (3-5 times)");
		chatOffenseSev2Lore.add("�7Light Advertising");
		chatOffenseSev2Lore.add("�7Trolling");
		chatOffenseSev2Lore.add("�7Constantly talking garbage");
		chatOffenseSev2Lore.add("�7Pestering staff");
		chatOffenseSev2Lore.add("�7Accusing a player of hacks in chat");
		chatOffenseSev2Lore.add(" ");
		chatOffenseSev2Lore.add("�5�nUse Severity 2 if many Severity 1 past offences");
		chatOffenseSev2Meta.setLore(chatOffenseSev2Lore);
		chatOffenseSev2.setItemMeta(chatOffenseSev2Meta);
		punishMenu.setItem(21, chatOffenseSev2);
		
		ItemStack chatOffenseSev3 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta chatOffenseSev3Meta = chatOffenseSev3.getItemMeta();
		chatOffenseSev3Meta.setDisplayName("�c�lSeverity 3");
		ArrayList<String> chatOffenseSev3Lore = new ArrayList<String>();
		chatOffenseSev3Lore.add("�9�lMute Duration: �7Permanent");
		chatOffenseSev3Lore.add(" ");
		chatOffenseSev3Lore.add("�f�lExamples:");
		chatOffenseSev3Lore.add("�7Spamming same thing in chat (15+ times)");
		chatOffenseSev3Lore.add("�7Strong Advertising");
		chatOffenseSev3Lore.add("�7Severe chat abuse towards players/staff");
		chatOffenseSev3Meta.setLore(chatOffenseSev3Lore);
		chatOffenseSev3.setItemMeta(chatOffenseSev3Meta);
		punishMenu.setItem(22, chatOffenseSev3);
		
		ItemStack generalOffense = new ItemStack(Material.SIGN, 1);
		ItemMeta generalOffenseMeta = generalOffense.getItemMeta();
		generalOffenseMeta.setDisplayName("�6�lGeneral Offense");
		ArrayList<String> generalOffenseLore = new ArrayList<String>();
		generalOffenseLore.add("�7Commmand/Map/Class/Skill exploits, etc");
		generalOffenseMeta.setLore(generalOffenseLore);
		generalOffense.setItemMeta(generalOffenseMeta);
		punishMenu.setItem(28, generalOffense);

		ItemStack generalOffenseSev1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta generalOffenseSev1Meta = generalOffenseSev1.getItemMeta();
		generalOffenseSev1Meta.setDisplayName("�a�lSeverity 1");
		ArrayList<String> generalOffenseSev1Lore = new ArrayList<String>();
		generalOffenseSev1Lore.add("�9�lBan Duration: �72 Hours");
		generalOffenseSev1Lore.add(" ");
		generalOffenseSev1Lore.add("�f�lExamples:");
		generalOffenseSev1Lore.add("�7Trolling (Gameplay Only)");
		generalOffenseSev1Lore.add(" ");
		generalOffenseSev1Lore.add("�5�nUse Severity 2 if many Severity 1 past offences");
		generalOffenseSev1Meta.setLore(generalOffenseSev1Lore);
		generalOffenseSev1.setItemMeta(generalOffenseSev1Meta);
		punishMenu.setItem(29, generalOffenseSev1);
		
		ItemStack generalOffenseSev2 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4);
		ItemMeta generalOffenseSev2Meta = generalOffenseSev2.getItemMeta();
		generalOffenseSev2Meta.setDisplayName("�e�lSeverity 2");
		ArrayList<String> generalOffenseSev2Lore = new ArrayList<String>();
		generalOffenseSev2Lore.add("�9�lBan Duration: �724 Hours");
		generalOffenseSev2Lore.add(" ");
		generalOffenseSev2Lore.add("�f�lExamples:");
		generalOffenseSev2Lore.add("�7Team Killing");
		generalOffenseSev2Lore.add("�7Block Glitching");
		generalOffenseSev2Lore.add("�7Lagging Server");
		generalOffenseSev2Lore.add(" ");
		generalOffenseSev2Lore.add("�5�nUse Severity 3 if many Severity 2 past offences");
		generalOffenseSev2Meta.setLore(generalOffenseSev2Lore);
		generalOffenseSev2.setItemMeta(generalOffenseSev2Meta);
		punishMenu.setItem(30, generalOffenseSev2);
		
		ItemStack generalOffenseSev3 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta generalOffenseSev3Meta = generalOffenseSev3.getItemMeta();
		generalOffenseSev3Meta.setDisplayName("�c�lSeverity 3");
		ArrayList<String> generalOffenseSev3Lore = new ArrayList<String>();
		generalOffenseSev3Lore.add("�9�lBan Duration: �7Permanent");
		generalOffenseSev3Lore.add(" ");
		generalOffenseSev3Lore.add("�f�lExamples:");
		generalOffenseSev3Lore.add("�7Repeatedly crashing server with glitch");
		generalOffenseSev3Meta.setLore(generalOffenseSev3Lore);
		generalOffenseSev3.setItemMeta(generalOffenseSev3Meta);
		punishMenu.setItem(31, generalOffenseSev3);
		
		ItemStack clientMod = new ItemStack(Material.SIGN, 1);
		ItemMeta clientModMeta = clientMod.getItemMeta();
		clientModMeta.setDisplayName("�6�lClient Mod");
		ArrayList<String> clientModLore = new ArrayList<String>();
		clientModLore.add("�7X-ray, Forcefield, Speed, Fly, Inventory Hacks, etc");
		clientModMeta.setLore(clientModLore);
		clientMod.setItemMeta(clientModMeta);
		punishMenu.setItem(37, clientMod);

		ItemStack clientModSev1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta clientModSev1Meta = clientModSev1.getItemMeta();
		clientModSev1Meta.setDisplayName("�a�lSeverity 1");
		ArrayList<String> clientModSev1Lore = new ArrayList<String>();
		clientModSev1Lore.add("�9�lBan Duration: �712 Hours");
		clientModSev1Lore.add(" ");
		clientModSev1Lore.add("�f�lExamples:");
		clientModSev1Lore.add("�7Damage Indicators");
		clientModSev1Lore.add("�7Better Sprint");
		clientModSev1Lore.add("�7Minimaps");
		clientModSev1Lore.add(" ");
		clientModSev1Lore.add("�5�nUse this for 1st Offence");
		clientModSev1Meta.setLore(clientModSev1Lore);
		clientModSev1.setItemMeta(clientModSev1Meta);
		punishMenu.setItem(38, clientModSev1);
		
		ItemStack clientModSev2 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4);
		ItemMeta clientModSev2Meta = clientModSev2.getItemMeta();
		clientModSev2Meta.setDisplayName("�e�lSeverity 2");
		ArrayList<String> clientModSev2Lore = new ArrayList<String>();
		clientModSev2Lore.add("�9�lBan Duration: �71 week");
		clientModSev2Lore.add(" ");
		clientModSev2Lore.add("�f�lExamples:");
		clientModSev2Lore.add("�7Damage Indicators");
		clientModSev2Lore.add("�7Better Sprint");
		clientModSev2Lore.add("�7Minimaps");
		clientModSev2Lore.add(" ");
		clientModSev2Lore.add("�5�nUse this for 2nd Offence");
		clientModSev2Meta.setLore(clientModSev2Lore);
		clientModSev2.setItemMeta(clientModSev2Meta);
		punishMenu.setItem(39, clientModSev2);
		
		ItemStack clientModSev3 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta clientModSev3Meta = clientModSev3.getItemMeta();
		clientModSev3Meta.setDisplayName("�c�lSeverity 3");
		ArrayList<String> clientModSev3Lore = new ArrayList<String>();
		clientModSev3Lore.add("�9�lBan Duration: �7Permanent");
		clientModSev3Lore.add(" ");
		clientModSev3Lore.add("�f�lExamples:");
		clientModSev3Lore.add("�7Fly Hack");
		clientModSev3Lore.add("�7Speed Hack");
		clientModSev3Lore.add("�7Forefield");
		clientModSev3Lore.add(" ");
		clientModSev3Lore.add("�5�nMust be 100% sure they are hacking");
		clientModSev3Meta.setLore(clientModSev3Lore);
		clientModSev3.setItemMeta(clientModSev3Meta);
		punishMenu.setItem(40, clientModSev3);
		
		ItemStack warn = new ItemStack(Material.PAPER, 1);
		ItemMeta warnMeta = warn.getItemMeta();
		warnMeta.setDisplayName("�c�lWarn");
		ArrayList<String> warnLore = new ArrayList<String>();
		warnLore.add("�7Warn the player that they have broken a rule and is being watched");
		warnMeta.setLore(warnLore);
		warn.setItemMeta(warnMeta);
		punishMenu.setItem(25, warn);
		
		ItemStack mute = new ItemStack(Material.EMERALD_BLOCK, 1);
		ItemMeta muteMeta = mute.getItemMeta();
		muteMeta.setDisplayName("�c�lPermanent Mute");
		ArrayList<String> muteLore = new ArrayList<String>();
		muteLore.add("�9�lMute Duration: �7Permanent");
		muteMeta.setLore(muteLore);
		mute.setItemMeta(muteMeta);
		punishMenu.setItem(34, mute);
		
		ItemStack ban = new ItemStack(Material.REDSTONE_BLOCK, 1);
		ItemMeta banMeta = ban.getItemMeta();
		banMeta.setDisplayName("�c�lPermanent Ban");
		ArrayList<String> banLore = new ArrayList<String>();
		banLore.add("�9�lBan Duration: �7Permanent");
		banMeta.setLore(banLore);
		ban.setItemMeta(banMeta);
		punishMenu.setItem(43, ban);
		
		player.openInventory(punishMenu);
	}
	
	public static HashMap<String, String> punish = new HashMap<String, String>();
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if (!(event.getInventory().getTitle().equals("Punish Menu"))) return;
		
		punish.remove(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		if (!(event.getInventory().getTitle().equals("Punish Menu"))) return;
		if (!(event.getCurrentItem().hasItemMeta())) return;
		
		Player player = (Player) event.getWhoClicked();
		Player target = Bukkit.getPlayer(punish.get(player.getName()));
		
		Account targetAccount = new Account(target);
		
		event.setCancelled(true);
		
		if (event.getSlot() == 4) { //Chat Offense - Severity 1 - 2
			targetAccount.punish(PunishType.KICK, 0, player.getName(), "You have been kicked", false);
		} else if (event.getSlot() == 20) { //Chat Offense - Severity 1 - 2
			targetAccount.punish(PunishType.TEMPMUTE, 7200, player.getName(), "N/A", false);
		} else if (event.getSlot() == 21) { //Chat Offense - Severity 2 - 24
			targetAccount.punish(PunishType.TEMPMUTE, 86400, player.getName(), "N/A", false);
		} else if (event.getSlot() == 22) { //Chat Offense - Severity 3 - Perm
			targetAccount.punish(PunishType.MUTE, -1, player.getName(), "N/A", false);
		} else if (event.getSlot() == 22) { //General Offense - Severity 1 - 2 hours
			targetAccount.punish(PunishType.TEMPBAN, 7200, player.getName(), "N/A", true);
		} else if (event.getSlot() == 22) { //General Offense - Severity 2 24 Hours
			targetAccount.punish(PunishType.TEMPBAN, 86400, player.getName(), "N/A", true);
		} else if (event.getSlot() == 22) { //General Offense - Severity 3 - Perm
			targetAccount.punish(PunishType.BAN, -1, player.getName(), "N/A", true);
		} else if (event.getSlot() == 22) { //Client Mod - Severity 1 - 12 Hours
			targetAccount.punish(PunishType.TEMPBAN, 43200, player.getName(), "N/A", true);
		} else if (event.getSlot() == 22) { //Client Mod - Severity 2 - 1 Week
			targetAccount.punish(PunishType.TEMPBAN, 604800, player.getName(), "N/A", true);
		} else if (event.getSlot() == 22) { //Client Mod - Severity 3 - Perm
			targetAccount.punish(PunishType.BAN, -1, player.getName(), "N/A", true);
		} else if (event.getSlot() == 25) { //Warn
			targetAccount.punish(PunishType.WARN, 0, player.getName(), "N/A", false);
		} else if (event.getSlot() == 34) { //Permanent Mute
			targetAccount.punish(PunishType.MUTE, -1, player.getName(), "N/A", false);
		} else if (event.getSlot() == 43) { //Permanent Ban
			targetAccount.punish(PunishType.BAN, -1, player.getName(), "N/A", true);
		}
	}

}
