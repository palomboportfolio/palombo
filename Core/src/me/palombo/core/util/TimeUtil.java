package me.palombo.core.util;

public class TimeUtil {
	
	public static String formatTime(int secs) {
		int hours = secs / 3600;
		int remainder = secs % 3600;
		int minutes = remainder / 60;
		int seconds = remainder % 60;

		if (hours > 0) {
			return hours + ":" + minutes + ":" + seconds;
		} else if (minutes > 0) {
			if (seconds < 10) {
				return minutes + ":0" + seconds;
			} else {
				return minutes + ":" + seconds;
			}
		} else {
			return String.valueOf(seconds);
		}
	}

}
