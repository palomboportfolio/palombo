package me.palombo.core.account.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class StaffRankCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("staffrank")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args[0].equalsIgnoreCase("list")) {
							player.sendMessage("�8�m�l------------------------------");
							player.sendMessage("  " + ChatColor.RED.toString() + ChatColor.BOLD + "Owner");
							player.sendMessage("  " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Lead Developer");
							player.sendMessage("  " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Developer");
							player.sendMessage("  " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Jr. Developer");
							player.sendMessage("  " + ChatColor.GREEN.toString() + ChatColor.BOLD + "Admin");
							player.sendMessage("  " + ChatColor.GOLD.toString() + ChatColor.BOLD + "Mod+");
							player.sendMessage("  " + ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Mod");
							player.sendMessage("  " + ChatColor.BLUE.toString() + ChatColor.BOLD + "Helper");
							player.sendMessage("  " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "You" + ChatColor.WHITE.toString() + ChatColor.BOLD + "Tube");
							player.sendMessage("�8�m�l------------------------------");
						} else if (args[0].equalsIgnoreCase("get")) {
							if (args.length < 2) {
								player.sendMessage("�c�lRank �7/staffrank get [player]");
							} else {
								Player target = Bukkit.getPlayer(args[1]);
								Account targetAccount = new Account(target);

								if (targetAccount.isInDatabase()) {
									player.sendMessage("�c�lStaff Rank �7" + args[1] + " has the rank �8[�c�l" + targetAccount.getStaffRank() + "�8]�7.");
								} else {
									player.sendMessage("�c�lStaff Rank �7" + args[1] + " was not found.");
								}
							}
						} else if (args[0].equalsIgnoreCase("set")) {
							if (args.length < 3) {
								player.sendMessage("�c�lStaff Rank �7/staffrank set [player] [rank]");
							} else {
								Player target = Bukkit.getPlayer(args[1]);
								Account targetAccount = new Account(target);

								if (!(targetAccount.isInDatabase())) {
									player.sendMessage("�c�lStaff Rank �7" + args[1] + " was not found.");
								} else {
									if (args[2].equalsIgnoreCase(StaffRank.OWNER.toString())) {
										targetAccount.setStaffRank(StaffRank.OWNER);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lOWNER�8]�7.");
									}  else if (args[2].equalsIgnoreCase(StaffRank.LEADDEV.toString())) {
										targetAccount.setStaffRank(StaffRank.LEADDEV);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lLEADDEV�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.DEV.toString())) {
										targetAccount.setStaffRank(StaffRank.DEV);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lDEV�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.JRDEV.toString())) {
										targetAccount.setStaffRank(StaffRank.JRDEV);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lJRDEV�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.ADMIN.toString())) {
										targetAccount.setStaffRank(StaffRank.ADMIN);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lADMIN�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.MODPLUS.toString())) {
										targetAccount.setStaffRank(StaffRank.MODPLUS);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lMOD+�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.MOD.toString())) {
										targetAccount.setStaffRank(StaffRank.MOD);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lMOD�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.HELPER.toString())) {
										targetAccount.setStaffRank(StaffRank.HELPER);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lHELPER�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.YOUTUBE.toString())) {
										targetAccount.setStaffRank(StaffRank.YOUTUBE);
										player.sendMessage("�c�lStaff Rank �7" + args[1] + " now has the rank �8[�c�lYOUTUBE�8]�7.");
									} else if (args[2].equalsIgnoreCase(StaffRank.NONE.toString())) {
										targetAccount.setStaffRank(StaffRank.NONE);
										player.sendMessage("�c�lStaff Rank �7 You no longer have a staff rank.");
									} else {
										player.sendMessage("�c�lStaff Rank �7" + args[2] + " is not a valid rank.");
									}
								}
							}
						} else {
							usageMessage(player);
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/staffrank list");
		player.sendMessage("  �7/staffrank get [player]");
		player.sendMessage("  �7/staffrank set [player] [rank]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
