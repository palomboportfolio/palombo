package me.palombo.core.account.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.DonorRank;
import me.palombo.core.rank.StaffRank;

public class DonorRankCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("donorrank")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
				
				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args[0].equalsIgnoreCase("list")) {
							player.sendMessage("�8�m�l------------------------------");
							player.sendMessage("  " + ChatColor.AQUA.toString() + ChatColor.BOLD + "Diamond");
							player.sendMessage("  " + ChatColor.GREEN.toString() + ChatColor.BOLD + "Emerald");
							player.sendMessage("  " + ChatColor.GOLD.toString() + ChatColor.BOLD + "Gold");
							player.sendMessage("  " + ChatColor.GRAY.toString() + ChatColor.BOLD + "Iron");
							player.sendMessage("  " + ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "Coal");
							player.sendMessage("  " + ChatColor.GRAY.toString() + "Member");
							player.sendMessage("�8�m�l------------------------------");
						} else if (args[0].equalsIgnoreCase("get")) {
							if (args.length < 2) {
								player.sendMessage("�c�lDonor �7/donorrank get [player]");
							} else {
								Player target = Bukkit.getPlayer(args[1]);
								Account targetAccount = new Account(target);

								if (targetAccount.isInDatabase()) {
									player.sendMessage("�c�lDonor Rank �7" + args[1] + " has the rank �8[�c�l" + targetAccount.getStaffRank() + "�8]�7.");
								} else {
									player.sendMessage("�c�lDonor Rank �7" + args[1] + " was not found.");
								}
							}
						} else if (args[0].equalsIgnoreCase("set")) {
							if (args.length < 3) {
								player.sendMessage("�c�lDonor Rank �7/donorrank set [player] [rank]");
							} else {
								Player target = Bukkit.getPlayer(args[1]);
								Account targetAccount = new Account(target);

								if (!(targetAccount.isInDatabase())) {
									player.sendMessage("�c�lDonor Rank �7" + args[1] + " was not found.");
								} else {
									if (args[2].equalsIgnoreCase(DonorRank.DIAMOND.toString())) {
										targetAccount.setDonorRank(DonorRank.DIAMOND);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lDIAMOND�8]�7.");
									} else if (args[2].equalsIgnoreCase(DonorRank.EMERALD.toString())) {
										targetAccount.setDonorRank(DonorRank.EMERALD);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lEMERALD�8]�7.");
									} else if (args[2].equalsIgnoreCase(DonorRank.GOLD.toString())) {
										targetAccount.setDonorRank(DonorRank.GOLD);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lGOLD�8]�7.");
									} else if (args[2].equalsIgnoreCase(DonorRank.IRON.toString())) {
										targetAccount.setDonorRank(DonorRank.IRON);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lIRON�8]�7.");
									} else if (args[2].equalsIgnoreCase(DonorRank.COAL.toString())) {
										targetAccount.setDonorRank(DonorRank.COAL);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lCOAL�8]�7.");
									} else if (args[2].equalsIgnoreCase(DonorRank.DEFAULT.toString())) {
										targetAccount.setDonorRank(DonorRank.DEFAULT);
										player.sendMessage("�c�lDonor Rank �7" + args[1] + " now has the rank �8[�c�lMEMBER�8]�7.");
									} else {
										player.sendMessage("�c�lDonor Rank �7" + args[2] + " is not a valid rank.");
									}
								}
							}
						} else {
							usageMessage(player);
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/donorrank list");
		player.sendMessage("  �7/donorrank get [player]");
		player.sendMessage("  �7/donorrank set [player] [rank]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
