package me.palombo.core.account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import me.palombo.core.Core;
import me.palombo.core.Epoch;
import me.palombo.core.database.MySQL;
import me.palombo.core.punish.PunishType;

import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuit implements Listener {

	@EventHandler
	public void onPunishCheck(AsyncPlayerPreLoginEvent event) {
		try {
			ResultSet resultSet = MySQL.con.createStatement().executeQuery(
					"SELECT * FROM Punishments WHERE UUID = '" + event.getUniqueId().toString() + "'");

			while (resultSet.next()) {
				PunishType type = PunishType.valueOf(resultSet.getString("Type"));
				String timeStamp = resultSet.getString("TimeStamp");
				int endTime = resultSet.getInt("EndTime");
				if (endTime == 0) continue;
				int oldEpoch = (int)Timestamp.valueOf(timeStamp).getTime() / 1000;
				int totalTime = oldEpoch + endTime;
				if (endTime != -1 && totalTime <= Epoch.getEpoch()) continue;
				String punisher = resultSet.getString("Punisher");
				String reason = resultSet.getString("Reason");
				String time = endTime == -1 ? "�cPERMANENT" : Core.timeFormat(totalTime - Epoch.getEpoch(), "�c");
				switch (type) {
				case BAN: 
				case TEMPBAN: {
                    String kickReason =
        					"�8�m�l------------------------------\n" +
        					"�c�lYou are banned.\n" +
        					"\n " +
        					"  �7Duration: �c" + time + "\n" +
        					//"�7Reason �c" + reason +
        					"�7Punished By: �c" + punisher + "\n" +
        					"�8�m�l------------------------------";
                    
					event.disallow(Result.KICK_BANNED, kickReason);
					return;
				}
				case MUTE: 
				case TEMPMUTE: {
					HashMap<String, String> storage = new HashMap<String, String>();
					storage.put("punisher", punisher);
					storage.put("reason", reason);
					if (endTime == -1) {
						storage.put("time", "-1");
					} else {
						storage.put("time", Integer.toString(totalTime));
					}
					Core.muted.put(event.getUniqueId(), storage);
				}
				}
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}


	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		Account account = new Account(player);
		
		account.loadAccount();
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		Account account = new Account(player);

		account.loggedOff();
	}
}
