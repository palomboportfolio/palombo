package me.palombo.core.account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.mysql.jdbc.PreparedStatement;

import me.palombo.core.Core;
import me.palombo.core.Epoch;
import me.palombo.core.database.MySQL;
import me.palombo.core.punish.PunishType;
import me.palombo.core.rank.DonorRank;
import me.palombo.core.rank.StaffRank;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;

public class Account implements Listener {

	private Player player;

	public Account(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void loadAccount() {
		if (isInDatabase()) {
			Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), () -> {
				try {
					
					ResultSet r = MySQL.con.createStatement().executeQuery(
							"SELECT * FROM Accounts WHERE UUID = '" + this.player.getUniqueId().toString() + "'");
					
					if (r.next()) {
						Core.staffrank.put(this.player.getUniqueId(), StaffRank.valueOf(r.getString("StaffRank")));
						Core.donorrank.put(this.player.getUniqueId(), DonorRank.valueOf(r.getString("DonorRank")));
						Core.coins.put(this.player.getUniqueId(), r.getInt("Coins"));
						Core.level.put(this.player.getUniqueId(), r.getInt("Level"));
					}
								
					r.close();
				} catch(Exception e) {
					System.out.println("Could not load player: " + this.player.getName() + ".");
					e.printStackTrace();
				}
			});
		} else {
			createPlayerData();
			System.out.println("4");
		}
	}
	
	public boolean isInDatabase() {
		try {
			PreparedStatement sql = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT * FROM `Accounts` WHERE UUID=?;");
			sql.setString(1, this.player.getUniqueId().toString());

			ResultSet resultSet = sql.executeQuery();
			boolean contains = resultSet.next();

			sql.close();
			resultSet.close();

			return contains;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void createPlayerData() {
		try {
				PreparedStatement newPlayer = (PreparedStatement) MySQL.con
						.prepareStatement("INSERT INTO `Accounts` values(?,?,?,?,0,0,?,0);");
				
				newPlayer.setString(1, this.player.getUniqueId().toString());
				newPlayer.setString(2, this.player.getName());
				newPlayer.setString(3, StaffRank.NONE.toString());
				newPlayer.setString(4, DonorRank.DEFAULT.toString());
				newPlayer.setString(5, this.player.getAddress().toString());
				
				newPlayer.execute();
				
				loadAccount();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loggedOff() {
		Core.staffrank.remove(this.player.getName());
		Core.donorrank.remove(this.player.getName());
		Core.coins.remove(this.player.getName());
		Core.level.remove(this.player.getName());
	}

	public String getName() {
		return this.player.getName();
	}

	public void sendMessage(String message) {
		this.player.sendMessage(message);
	}

	public void sendTitle(String titleMessage, String subtitleMessage) {
		PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE,
				IChatBaseComponent.ChatSerializer
				.a("{\"text\":\"TITTLEMESSAGE\"}".replace("TITTLEMESSAGE", titleMessage)),
				20, 40, 20);
		PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE,
				IChatBaseComponent.ChatSerializer
				.a("{\"text\":\"SUBTITLEMESSAGE\"}".replace("SUBTITLEMESSAGE", subtitleMessage)),
				20, 40, 20);

		((CraftPlayer) this.player.getPlayer()).getHandle().playerConnection.sendPacket(title);
		((CraftPlayer) this.player.getPlayer()).getHandle().playerConnection.sendPacket(subtitle);
	}

	public void sendActionbar(String actionbarMessage) {
		PacketPlayOutChat actiobar = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer
				.a("{\"text\":\"ACTIONBARMESSAGE\"}".replace("ACTIONBARMESSAGE", actionbarMessage)), (byte) 2);

		((CraftPlayer) this.player).getHandle().playerConnection.sendPacket(actiobar);
	}

	public void setName(String name) {
		try {
			PreparedStatement sql = (PreparedStatement) MySQL.con
					.prepareStatement("SELECT Name FROM `Account` WHERE UUID=?;");
			sql.setString(1, this.player.getUniqueId().toString());
			PreparedStatement RankSave = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `Account` SET Name=? WHERE UUID=?;");
			RankSave.setString(1, name);
			RankSave.setString(2, this.player.getUniqueId().toString());
			RankSave.executeUpdate();
			RankSave.close();
			sql.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void setStaffRank(StaffRank rank) {
		try {
			PreparedStatement RankSave = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `Accounts` SET StaffRank=? WHERE UUID=?;");

			RankSave.setString(1, rank.toString());
			RankSave.setString(2, this.player.getUniqueId().toString());

			RankSave.executeUpdate();
			RankSave.close();

			Core.staffrank.replace(this.player.getUniqueId(), rank);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setDonorRank(DonorRank rank) {
		try {
			PreparedStatement RankSave = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `Accounts` SET DonorRank=? WHERE UUID=?;");

			RankSave.setString(1, rank.toString());
			RankSave.setString(2, this.player.getUniqueId().toString());

			RankSave.executeUpdate();
			RankSave.close();

			Core.donorrank.replace(this.player.getUniqueId(), rank);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setCoins(int coins) {
		try {
			PreparedStatement RankSave = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `Accounts` SET Coins=? WHERE UUID=?;");

			RankSave.setInt(1, coins);
			RankSave.setString(2, this.player.getUniqueId().toString());

			RankSave.executeUpdate();
			RankSave.close();

			Core.coins.replace(this.player.getUniqueId(), coins);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLevel(int level) {
		try {
			PreparedStatement RankSave = (PreparedStatement) MySQL.con
					.prepareStatement("UPDATE `Accounts` SET Level=? WHERE UUID=?;");

			RankSave.setInt(1, level);
			RankSave.setString(2, this.player.getUniqueId().toString());

			RankSave.executeUpdate();
			RankSave.close();

			Core.level.replace(this.player.getUniqueId(), level);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public StaffRank getStaffRank() {
		return Core.staffrank.get(this.player.getUniqueId());
	}

	public DonorRank getDonorRank() {
		return Core.donorrank.get(this.player.getUniqueId());
	}

	public int getCoins() {
		return Core.coins.get(this.player.getUniqueId());
	}
	
	public int getLevel() {
		return Core.level.get(this.player.getUniqueId());
	}

	public boolean hasStaffRank(StaffRank rank, boolean inform) {
		if (getStaffRank().compareTo(rank) <= 0) {
			return true;
		}
		
		if (inform) {
			player.sendMessage("�c�lPermissions �7You must have at least �8[�c�l" + rank + "�8]�7.");
		}

		return false;
	}

	public boolean hasDonorRank(DonorRank rank, boolean inform) {
		if (getDonorRank().compareTo(rank) <= 0) {
			return true;
		}
		if (inform) {
			player.sendMessage("�c�lPermissions �7You must have at least �8[�c�l" + rank + "�8]�7.");
		}

		return false;
	}

	public int getPing() {
		try {
			String bukkitversion = Bukkit.getServer().getClass().getPackage().getName().substring(23);
			Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit." + bukkitversion + ".entity.CraftPlayer");
			Object handle = craftPlayer.getMethod("getHandle").invoke(this.player);
			Integer ping = (Integer) handle.getClass().getDeclaredField("ping").get(handle);

			return ping.intValue();
		} catch (Exception e) {
			return -1;
		}
	}
	
    public void checkPunished(ResultSet resultSet) {
        try {
            while (resultSet.next()) {
                PunishType type = PunishType.valueOf(resultSet.getString("Type"));
                String timeStamp = resultSet.getString("TimeStamp");
                int endTime = resultSet.getInt("EndTime");
                if (endTime == 0) continue;
                int oldEpoch = (int)Timestamp.valueOf(timeStamp).getTime() / 1000;
                int totalTime = oldEpoch + endTime;
                if (endTime != -1 && totalTime <= Epoch.getEpoch()) continue;
                String punisher = resultSet.getString("Punisher");
                String reason = resultSet.getString("Reason");
                String time = endTime == -1 ? "�cPERMANENT" : Core.timeFormat(totalTime - Epoch.getEpoch(), "�c");
                switch (type) {
                    case BAN: 
                    case TEMPBAN: {
                        String kickReason =
            					"�8�m�l------------------------------\n" +
                    					"�c�lYou are banned.\n" +
                    					"\n" +
                    					"  �7Duration: �c" + time + " \n" +
                    					//"�7Reason �c" + reason +
                    					"�7Punished By: �c" + punisher + " \n" +
                    					"�8�m�l------------------------------";
                    	this.player.kickPlayer(kickReason);
                        return;
                    }
                    case MUTE: 
                    case TEMPMUTE: {
                        HashMap<String, String> storage = new HashMap<String, String>();
                        storage.put("punisher", punisher);
                        storage.put("reason", reason);
                        if (endTime == -1) {
                            storage.put("time", "-1");
                        } else {
                            storage.put("time", Integer.toString(totalTime));
                        }
                        Core.muted.put(this.player.getUniqueId(), storage);
                    }
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void punish(PunishType type, int endTime, String punisher, String reason, Boolean kick) {
        try {
            Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
            PreparedStatement newPunishment = (PreparedStatement) MySQL.con.prepareStatement("INSERT INTO `Punishments` values(?,?,?,?,?,?,null);");
            
            newPunishment.setString(1, this.player.getUniqueId().toString());
            newPunishment.setString(2, type.toString());
            newPunishment.setInt(3, endTime);
            newPunishment.setTimestamp(4, timeStamp);
            newPunishment.setString(5, punisher);
            newPunishment.setString(6, reason);
            newPunishment.execute();
            
            String time = Core.timeFormat(endTime, "�c");
            
            if (type.equals(PunishType.MUTE)) {
                //RedisBungee.getApi().sendProxyCommand(RedisBungee.getApi().getProxy(uuid), "notify mute " + RedisBungee.getApi().getNameFromUuid(uuid) + " " + punisher + " " + (endTime + Epoch.getEpoch()) + " " + reason);
        		ResultSet resultSet = MySQL.con.createStatement().executeQuery(
        				"SELECT * FROM Punishments WHERE UUID = '" + this.player.getUniqueId().toString() + "'");
        		checkPunished(resultSet);
            }
            
            if (kick) {
            	
                String kickReason =
    					"�8�m�l------------------------------\n" +
    					"�c�lYou have been banned.\n" +
    					"\n" +
    					"  �7Duration: �c" + time + " \n" +
    					//"�7Reason �c" + reason +
    					"�7Punished By: �c" + punisher + " \n" +
    					"�8�m�l------------------------------";
                
                
                if (type.equals(PunishType.KICK)) {
                    kickReason =
        					"�8�m�l------------------------------" +
        					"�c�lYou have been kicked.\n" +
        					"\n" +
        					//"�7Reason �c" + reason +
        					"�7Punished By: �c" + punisher + " \n" +
        					"�8�m�l------------------------------";

                }
               
                this.player.kickPlayer(kickReason);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unPunish(PunishType type) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), () -> {
            try {
                String otherType = "TEMP" + type.toString();
                PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("UPDATE `Punishments` SET `ENDTIME`=? WHERE UUID=? AND TYPE=? || TYPE=?");
                sql.setString(4, otherType);
                sql.setString(3, type.toString());
                sql.setString(2, this.player.getUniqueId().toString());
                sql.setInt(1, 0);
                MySQL.update((PreparedStatement) sql);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        );
    }

    public void clearHistory() {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), () -> {
            try {
                PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("DELETE FROM `Punishments` WHERE UUID=?;");
                sql.setString(1, this.player.getUniqueId().toString());
                MySQL.update((PreparedStatement) sql);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        );
    }

    @SuppressWarnings("deprecation")
	public void history(Player receiver) {
        Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), () -> {
            try {
                PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("SELECT * FROM `Punishments` WHERE UUID=?;");
                sql.setString(1, this.player.getUniqueId().toString());
                ResultSet resultSet = sql.executeQuery();
                receiver.sendMessage("\u00a78\u00a7m----------------------\u00a77[\u00a7c\u00a7lHISTORY\u00a77]\u00a78\u00a7m----------------------");
                while (resultSet.next()) {
                    String timeStamp = resultSet.getString("TimeStamp");
                    PunishType type = PunishType.valueOf(resultSet.getString("Type"));
                    int endTime = resultSet.getInt("EndTime");
                    String punisher = resultSet.getString("Punisher");
                    String reason = resultSet.getString("Reason");
                    String time = Core.timeFormat(endTime, "\u00a7b");
                    String timeLength = "";
                    String color = "\u00a7c";
                    int oldEpoch = (int)Timestamp.valueOf(timeStamp).getTime() / 1000;
                    int totalTime = oldEpoch + endTime;
                    if (endTime == -1 || totalTime > Epoch.getEpoch()) {
                        color = "\u00a7a";
                    }
                    if (type.equals(PunishType.TEMPBAN) || type.equals(PunishType.TEMPMUTE)) {
                        timeLength = " (" + time + "\u00a77)";
                    }
                    //type = type.replace("ban", "bann");
                    //type = type.replace("mute", "mut");
                    Timestamp date = Timestamp.valueOf(timeStamp);
                    receiver.sendMessage("\u00a77" + date.getMonth() + "." + date.getDate() + ".20" + Integer.toString(date.getYear()).substring(1) + " \u00a78\u00a7l\u00bb " + color + type + "ed " + timeLength + "\u00a77 by " + color + punisher + "\u00a77 for: " + color + reason + "\u00a77.");
                }
                receiver.sendMessage("\u00a78\u00a7m-----------------------------------------------------");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        );
    }

}
