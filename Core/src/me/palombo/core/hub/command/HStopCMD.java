package me.palombo.core.hub.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.palombo.core.account.Account;
import me.palombo.core.hub.Hub;
import me.palombo.core.rank.StaffRank;

public class HStopCMD implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("hstop")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					Hub.stopHub();
				}

			} else {
				Hub.stopHub();
			}
		}
		return true;
	}
}
