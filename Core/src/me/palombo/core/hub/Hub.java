package me.palombo.core.hub;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.palombo.core.Core;

public class Hub {

	public static void stopHub() {
		Bukkit.broadcastMessage("�a�lThis server is updating! You will be sent to the lobby in 3 seconds so it can quickly restart!");
		Bukkit.getScheduler().runTaskLater(Core.getInstance(), new Runnable() {
			@Override
			public void run() {
				sendToHub();
				Bukkit.getScheduler().runTaskLater(Core.getInstance(), new Runnable() {
					@Override
					public void run() {
						Bukkit.shutdown();
					}
				}, 10);
			}
		}, 60);
	}

	@SuppressWarnings("rawtypes")
	public static Integer sendToHub() {
		Iterator var1 = Bukkit.getOnlinePlayers().iterator();

		while(var1.hasNext()) {
			Player p = (Player)var1.next();
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("Connect");
			out.writeUTF("hub");
			p.sendPluginMessage(Core.getInstance(), "BungeeCord", out.toByteArray());
		}

		return 0;

	}

}
