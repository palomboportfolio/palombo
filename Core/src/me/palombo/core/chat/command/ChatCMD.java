package me.palombo.core.chat.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.chat.Chat;
import me.palombo.core.rank.StaffRank;

public class ChatCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("chat")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.HELPER, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args[0].equalsIgnoreCase("toggle")) {
							if (Chat.chatEnabled()) {
								Bukkit.broadcastMessage("�c�lChat �7The chat has been �4�ldisabled�7.");
								Chat.chatEnabled = false;
							} else {
								Bukkit.broadcastMessage("�c�lChat �7The chat has been �a�lenabled�7.");
								Chat.chatEnabled = true;
							}
						} else if (args[0].equalsIgnoreCase("slow")) {
							player.sendMessage("�cNeeds to be done.");
							/*if (args.length == 1) {
								player.sendMessage("�c�lChat �7/chat slow [time]");
							} else {

							}*/
						} else if (args[0].equalsIgnoreCase("announce")) {
							if (args.length == 1) {
								player.sendMessage("�c�lChat �7/chat announce [message]");
							} else {
								String message = "";
								for (int i = 1; i < args.length; i++) {
									message = message + args[i] + " ";
								}
								
								Bukkit.broadcastMessage("�c�l>> �4�l" + player.getName() + " �f�l" + message);
							}
						} else {
							usageMessage(player);
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/chat toggle");
		player.sendMessage("  �7/chat slow [time]");
		player.sendMessage("  �7/chat announce [message]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
