package me.palombo.core.chat;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.palombo.core.Core;
import me.palombo.core.Epoch;
import me.palombo.core.account.Account;
import me.palombo.core.rank.DonorRank;
import me.palombo.core.rank.StaffRank;

public class Chat implements Listener {
	
	public static boolean chatEnabled = true;
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		Account account = new Account(player);
		StaffRank staffrank = account.getStaffRank();
		DonorRank donorrank = account.getDonorRank();
		
		if (Core.muted.containsKey(player.getUniqueId())) {
            HashMap<String, String> storage = Core.muted.get(player.getUniqueId());
            Long time = Long.valueOf(storage.get("time"));
            String punisher = storage.get("punisher");
            String reason = storage.get("reason");
            if (time == -1) {
                event.setCancelled(true);
                player.sendMessage("�8�m�l------------------------------");
                player.sendMessage("�c�lYou are muted.");
                player.sendMessage(" ");
                player.sendMessage("�7Duration: �cPermanent");
                //player.sendMessage("�7Reason: �c" + reason);
                player.sendMessage("�7Punished By: �c" + punisher);
                player.sendMessage("�8�m�l------------------------------");
            } else if (time > (long) Epoch.getEpoch()) {
                event.setCancelled(true);
                player.sendMessage("�8�m�l------------------------------");
                player.sendMessage("�c�lYou are muted.");
                player.sendMessage(" ");
                player.sendMessage("�7Duration: �cPermanent");
                //player.sendMessage("�7Reason: �c" + reason);
                player.sendMessage("�7Punished By: �c" + punisher);
                player.sendMessage("�8�m�l------------------------------");
            } else {
            	Core.muted.remove(player.getUniqueId());
            }
        }
		
		if (chatEnabled == false) {
			if (!(account.hasStaffRank(StaffRank.HELPER, false))) {
				event.setCancelled(true);
				player.sendMessage("�c�lChat �7The chat is currently disabled.");
				return;
			}
		}
		
		if (staffrank == StaffRank.NONE) {
			if (donorrank == DonorRank.DEFAULT) {
				event.setFormat("�7" + account.getLevel() + " �7" + player.getName() + " �8� �7" + event.getMessage());
			} else {
				event.setFormat("�7" + account.getLevel() + " " + donorrank.getPrefix() + " �6" + player.getName() + " �8� �7" + event.getMessage());
			}
		} else {
			event.setFormat("�7" + account.getLevel() + " " + staffrank.getPrefix() + " �6" + player.getName() + " �8� �7" + event.getMessage());
		}
	}
	
	public static boolean chatEnabled() {
		if (chatEnabled) {
			return true;
		} else {
			return false;
		}
	}

}
