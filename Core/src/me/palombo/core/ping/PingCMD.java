package me.palombo.core.ping;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class PingCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ping")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);
					if (account.hasStaffRank(StaffRank.HELPER, true)) {
						if (args.length == 0) {
							player.sendMessage("�c�lPing �7Your ping is �7�l" + account.getPing() + "�7.");
						} else {
							if (Bukkit.getPlayer(args[0]) != null) {
								Player target = Bukkit.getPlayer(args[0]);
								Account targetAccount = new Account(target);
								
								player.sendMessage("�c�lPing �7" + args[0] + "'s ping is �7�l" + targetAccount.getPing() + "�7.");
							} else {
								player.sendMessage("�c�lPing �7" + args[0] + " �7was not found.");
							}
						}
					} else {
						player.sendMessage("�c�lPing �7Your ping is �7�l" + account.getPing() + "�7.");
					}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

}
