package me.palombo.core.level.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import me.palombo.core.account.Account;

public class LevelListener implements Listener {

	@EventHandler
	public void onLevelUp(PlayerLevelChangeEvent event) {
		Player player = event.getPlayer();
		Account account = new Account(player);

		if (event.getNewLevel() >= 100) {
			player.sendMessage("�c�lLevel �7You have reached the max level.");
			account.setLevel(100);
			player.setLevel(100);
		} else {
			player.sendMessage("�c�lLevel �7You have leveled up to level �7�l" + event.getNewLevel() + "�7.");
			player.setLevel(event.getNewLevel());
		}

		player.setExp(0);
	}


}
