package me.palombo.core.level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.Core;
import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;

public class LevelCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("level")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					if (args.length < 2) {
						usageMessage(player);
					} else {
						if (args.length >= 2) {
							if (args[0].equalsIgnoreCase("get") || args[0].equalsIgnoreCase("set")) {
								if (Bukkit.getPlayer(args[1]) != null) {
									Player target = Bukkit.getPlayer(args[1]);
									Account targetAccount = new Account(target);

									if (args[0].equalsIgnoreCase("get")) {
										player.sendMessage("�c�lLevel �7" + args[1] + " is level �7�l" + targetAccount.getLevel() + "�7.");
										return true;
									}

									if (args.length > 2) {
										if (Core.isInt(args[2])) {
											if (!(args[2].contains("-"))) {
												if (args[0].equalsIgnoreCase("set")) {
													targetAccount.setLevel(Integer.valueOf(args[2]));
													player.sendMessage("�c�lLevel �7" + args[1] + "'s level has been set to �7�l" + args[2] + "�7.");
													return true;
												}
											} else {
												player.sendMessage("�c�lLevel �7You must use a positive number for the amount.");
											}
										} else {
											player.sendMessage("�c�lLevel �7You must use a number for the amount.");
										}
									} else {
										usageMessage(player);
									}
								} else {
									player.sendMessage("�c�lLevel �7" + args[1] + " was not found.");
								}
							} else {
								usageMessage(player);
							}
						}
					}
				} else {
					player.sendMessage("�c�lLevel �7You are level �7�l" + account.getLevel() + "�7.");
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/level get [player]");
		player.sendMessage("  �7/level set [player] [amount]");
		player.sendMessage("�8�m�l------------------------------");
	}

}
