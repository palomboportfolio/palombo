package me.palombo.core.server.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ReloadConfirmMenu implements Listener {

	public static Inventory ReloadConfirmMenu;

	public static void openMenu(Player player) {
		ReloadConfirmMenu = Bukkit.createInventory(player, 9, "Reload Server Confirm Menu");

		ItemStack confirm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta confirmMeta = confirm.getItemMeta();
		confirmMeta.setDisplayName("�a�lConfirm");
		confirm.setItemMeta(confirmMeta);
		ReloadConfirmMenu.setItem(3, confirm);

		ItemStack noconfirm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta noconfirmMeta = noconfirm.getItemMeta();
		noconfirmMeta.setDisplayName("�c�lCancel");
		noconfirm.setItemMeta(noconfirmMeta);
		ReloadConfirmMenu.setItem(5, noconfirm);

		player.openInventory(ReloadConfirmMenu);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();

		if (player.getOpenInventory().getTitle().contains("Reload Server Confirm Menu")) {

			if (!(event.getCurrentItem().hasItemMeta())) return;

			if (event.getCurrentItem().getItemMeta().getDisplayName().equals("�a�lConfirm")) {
				player.closeInventory();
				Bukkit.getServer().reload();
				Bukkit.broadcastMessage("�c�lServer �7Server Reloaded.");
			} else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lCancel")) {
				player.closeInventory();
			} else {
				return;
			}
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();

		if (message.contains("server") || message.contains("plugman")) return;
		
		if (message.contains("reload") || message.contains("rl")) {
			event.setCancelled(true);
			player.sendMessage("�c�lServer �7/server reload");
		}
	}

}
