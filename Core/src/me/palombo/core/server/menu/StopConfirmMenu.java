package me.palombo.core.server.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class StopConfirmMenu implements Listener {

	public static Inventory StopConfirmMenu;

	public static void openMenu(Player player) {
		StopConfirmMenu = Bukkit.createInventory(player, 9, "Stop Server Confirm Menu");

		ItemStack confirm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta confirmMeta = confirm.getItemMeta();
		confirmMeta.setDisplayName("�a�lConfirm");
		confirm.setItemMeta(confirmMeta);
		StopConfirmMenu.setItem(3, confirm);

		ItemStack noconfirm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta noconfirmMeta = noconfirm.getItemMeta();
		noconfirmMeta.setDisplayName("�c�lCancel");
		noconfirm.setItemMeta(noconfirmMeta);
		StopConfirmMenu.setItem(5, noconfirm);

		player.openInventory(StopConfirmMenu);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();


		if (player.getOpenInventory().getTitle().contains("Stop Server Confirm Menu")) {
			
			if (!(event.getCurrentItem().hasItemMeta())) return;

			event.setCancelled(true);

			if (event.getCurrentItem().getItemMeta().getDisplayName().equals("�a�lConfirm")) {
				player.closeInventory();
				Bukkit.broadcastMessage("�c�lServer �7Server Stopped.");
				Bukkit.getServer().shutdown();
			} else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("�c�lCancel")) {
				player.closeInventory();
			} else {
				return;
			}
		}
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();
		
		if (message.contains("server") || message.contains("plugman")) return;

		if (message.contains("stop")) {
			event.setCancelled(true);
			player.sendMessage("�c�lServer �7/server stop");
		}
	}

}
