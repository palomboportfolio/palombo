package me.palombo.core.server.command;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.palombo.core.account.Account;
import me.palombo.core.rank.StaffRank;
import me.palombo.core.server.menu.ReloadConfirmMenu;
import me.palombo.core.server.menu.StopConfirmMenu;
import me.palombo.core.util.HTTP;
import net.minecraft.server.v1_8_R3.MinecraftServer;

public class ServerCMD implements CommandExecutor {
	
	private static final int MegaBytes = 1024 * 1024;
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("server")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				Account account = new Account(player);

				if (account.hasStaffRank(StaffRank.ADMIN, true)) {
					if (args.length == 0) {
						usageMessage(player);
					} else {
						if (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("stop")) {
							if (account.hasStaffRank(StaffRank.DEV, true)) {
								if (args[0].equalsIgnoreCase("reload")) {
									ReloadConfirmMenu.openMenu(player);
									return true;
								}
								
								if (args[0].equalsIgnoreCase("stop")) {
									StopConfirmMenu.openMenu(player);
									return true;
								}
							}
						} else if (args[0].equalsIgnoreCase("stats")) {
							int online = Bukkit.getOnlinePlayers().size();
							String tps = new DecimalFormat("#.00").format(MinecraftServer.getServer().recentTps[0]); 
							Integer chunks_loaded_1 = 0;
							for (World w : Bukkit.getWorlds()){
								chunks_loaded_1 += w.getLoadedChunks().length;
							}
							String chunksloaded = Integer.toString(chunks_loaded_1);			
							String[] ip_1 = null;
							try {
							 ip_1 = HTTP.getText("http://ip-api.com/csv/").split(",");
							} catch (Exception e){ }
							String ip = ip_1[ip_1.length - 1];
							int port = Bukkit.getServer().getPort();
							String time = new SimpleDateFormat("h:m a").format(new Date());
							
					         int totalMemory = (int) Math.round((Runtime.getRuntime().totalMemory() / MegaBytes));
					         int freeMemory = (int) Math.round(Runtime.getRuntime().freeMemory() / MegaBytes);
					         int maxMemory = (int) Math.round(Runtime.getRuntime().maxMemory() / MegaBytes);
							
							player.sendMessage("�8�m�l------------------------------");
							player.sendMessage("  �cOnline: �7" + online);
							player.sendMessage("  �cTPS: �7" + tps);
							player.sendMessage("  �cChunks Loaded: �7" + chunksloaded);
							player.sendMessage("  �cIP: �7" + ip);
							player.sendMessage("  �cPort: �7" + port);
							player.sendMessage("  �cTime: �7" + time);
							player.sendMessage(" ");
							player.sendMessage("  �c�lMemory:");
							player.sendMessage("    �cAllocated: �7" + totalMemory);
							player.sendMessage("    �cFree: �7" + freeMemory);
							player.sendMessage("    �cMaximum: �7" + maxMemory);
							player.sendMessage("�8�m�l------------------------------");
						} else {
							usageMessage(player);
						}
					}
				}
			} else {
				sender.sendMessage("You must be a player to run this command.");
			}
		}
		return true;
	}

	public void usageMessage(Player player) {
		player.sendMessage("�8�m�l------------------------------");
		player.sendMessage("  �7/server stats");
		player.sendMessage("  �7/server reload");
		player.sendMessage("  �7/server stop");
		player.sendMessage("�8�m�l------------------------------");
	}
}
