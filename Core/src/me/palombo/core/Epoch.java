package me.palombo.core;

public class Epoch {
    public static int getEpoch() {
        return (int) System.currentTimeMillis() / 1000;
    }
}